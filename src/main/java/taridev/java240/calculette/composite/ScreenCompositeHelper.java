package taridev.java240.calculette.composite;

import taridev.swingvu.composite.AbstractCompositeHelper;

public class ScreenCompositeHelper extends AbstractCompositeHelper<ScreenComposite> {

    public ScreenCompositeHelper(ScreenComposite composite) {
        super(composite);
    }
}
