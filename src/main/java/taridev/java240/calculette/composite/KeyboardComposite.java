package taridev.java240.calculette.composite;

import taridev.java240.calculette.vo.CalculatorVo;
import taridev.java240.calculette.vo.OperatorEnum;
import taridev.swingvu.composite.AbstractCompositeWithHelper;
import taridev.swingvu.exception.AbstractBusinessException;
import taridev.swingvu.factory.GraphicFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyboardComposite extends AbstractCompositeWithHelper<CalculatorVo, KeyboardCompositeHelper> {

    private JButton[] digits;
    private JButton divideButton;
    private JButton multiplyButton;
    private JButton substractButton;
    private JButton signButton;
    private JButton commaButton;
    private JButton addButton;
    private JButton clearButton;
    private JButton initButton;
    private JButton resultButton;

    private static final int MAX_DIGIT = 10;

    public KeyboardComposite(CalculatorVo model) throws AbstractBusinessException {
        super(model);
    }

    @Override
    public void innerCreateContent() {
        JPanel keyboardPanel = new JPanel(new BorderLayout());
        JPanel mainPanel = new JPanel(new GridLayout(4, 4));
        JPanel rightPanel = new JPanel(new GridLayout(2, 1));
        JPanel clearOptionPanel = new JPanel(new GridLayout(2, 1));
        JPanel resultPanel = new JPanel(new GridLayout(1, 1));

        createDigitButtons();
        createOperatorButtons();

        // main panel
        mainPanel.add(digits[7]);
        mainPanel.add(digits[8]);
        mainPanel.add(digits[9]);
        mainPanel.add(divideButton);
        mainPanel.add(digits[4]);
        mainPanel.add(digits[5]);
        mainPanel.add(digits[6]);
        mainPanel.add(multiplyButton);
        mainPanel.add(digits[1]);
        mainPanel.add(digits[2]);
        mainPanel.add(digits[3]);
        mainPanel.add(substractButton);
        mainPanel.add(signButton);
        mainPanel.add(digits[0]);
        mainPanel.add(commaButton);
        mainPanel.add(addButton);

        // right panel
        clearOptionPanel.add(clearButton);
        clearOptionPanel.add(initButton);
        resultPanel.add(resultButton);
        rightPanel.add(clearOptionPanel);
        rightPanel.add(resultPanel);

        keyboardPanel.add(mainPanel, BorderLayout.CENTER);
        keyboardPanel.add(rightPanel, BorderLayout.EAST);

        setLayout(new BorderLayout());
        add(keyboardPanel, BorderLayout.CENTER);
    }

    @Override
    public void mapComponentsToListeners() {
        KeyboardCompositeHelper helper = getHelper();
        for (JButton button : digits) {
            button.addActionListener(e -> getHelper().digitAction(button.getText()));
        }
        addButton.addActionListener(e -> helper.operatorAction(OperatorEnum.ADD));
        substractButton.addActionListener(e -> helper.operatorAction(OperatorEnum.SUBSTRACT));
        multiplyButton.addActionListener(e -> helper.operatorAction(OperatorEnum.MULTIPLY));
        divideButton.addActionListener(e -> helper.operatorAction(OperatorEnum.DIVIDE));
        resultButton.addActionListener(e -> helper.operatorEqualAction());
        commaButton.addActionListener(e -> helper.comaAction());
        initButton.addActionListener(e -> helper.clearAction());
        clearButton.addActionListener(e -> helper.eraseAction());
        signButton.addActionListener(e -> helper.signAction());
        setFocusable(true);
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                String keyChar = Character.toString(e.getKeyChar());
                if (Character.isDigit(e.getKeyChar())) {
                    digits[Integer.parseInt(keyChar)].doClick();
                } else if ("=".equals(keyChar) || e.getKeyCode() == KeyEvent.VK_ENTER) {
                    resultButton.doClick();
                } else if ("+".equals(keyChar)) {
                    addButton.doClick();
                } else if ("-".equals(keyChar)) {
                    substractButton.doClick();
                } else if ("*".equals(keyChar)) {
                    multiplyButton.doClick();
                } else if ("/".equals(keyChar)) {
                    divideButton.doClick();
                } else if (".".equals(keyChar) || ",".equals(keyChar)) {
                    commaButton.doClick();
                }
            }
        });
    }

    private void createDigitButtons() {
        digits = new JButton[MAX_DIGIT];
        for (int i=0 ; i<MAX_DIGIT ; i++) {
            digits[i] = GraphicFactory.createButton(null, String.valueOf(i));
            digits[i].setFont(digits[i].getFont().deriveFont(20.5f));
            digits[i].setFocusable(false);
        }
    }

    private void createOperatorButtons() {
        divideButton = new JButton(OperatorEnum.DIVIDE.toString());
        divideButton.setFocusable(false);
        multiplyButton = new JButton(OperatorEnum.MULTIPLY.toString());
        multiplyButton.setFocusable(false);
        substractButton = new JButton(OperatorEnum.SUBSTRACT.toString());
        substractButton.setFocusable(false);
        signButton = new JButton(OperatorEnum.PLUS_MINUS.toString());
        signButton.setFocusable(false);
        commaButton = new JButton(OperatorEnum.COMMA.toString());
        commaButton.setFocusable(false);
        addButton = new JButton(OperatorEnum.ADD.toString());
        addButton.setFocusable(false);
        clearButton = new JButton(OperatorEnum.ERASE.toString());
        clearButton.setFocusable(false);
        initButton = new JButton(OperatorEnum.CLEAR.toString());
        initButton.setFocusable(false);
        resultButton = new JButton(OperatorEnum.EQUAL.toString());
        resultButton.setFocusable(false);
    }

}
