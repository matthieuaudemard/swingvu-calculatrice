package taridev.java240.calculette.composite;

import taridev.java240.calculette.vo.CalculatorVo;
import taridev.java240.calculette.vo.OperatorEnum;
import taridev.java240.calculette.vo.ResultVo;
import taridev.java240.calculette.vo.StateEnum;
import taridev.swingvu.composite.AbstractCompositeHelper;

public class KeyboardCompositeHelper extends AbstractCompositeHelper<KeyboardComposite> {

    private static final String MINUS = "-";

    public KeyboardCompositeHelper(KeyboardComposite composite) {
        super(composite);
    }

    public void digitAction(String digit) {
        CalculatorVo calculatorVo = (CalculatorVo) getVO();
        ResultVo resultVo = calculatorVo.getResultVo();
        if ("-0".compareTo(resultVo.getResult()) == 0){
            resultVo.setResult("-" + digit);
        }
        else if (calculatorVo.getState() == StateEnum.NEW || calculatorVo.getState() == StateEnum.RESULT) {
            resultVo.setResult(digit);
            calculatorVo.setState(StateEnum.WAITING);
        } else if ("0".compareTo(resultVo.getResult()) != 0){
            resultVo.setResult(calculatorVo.getResultVo().getResult() + digit);
        }
    }

    public void operatorAction(OperatorEnum operator) {
        CalculatorVo calculatorVo = (CalculatorVo) getVO();
        calculatorVo.setState(StateEnum.NEW);
        operatorEqualAction();
        calculatorVo.setOperator(operator);
        calculatorVo.setOperand(Double.parseDouble(calculatorVo.getResultVo().getResult()));
    }

    public void operatorEqualAction() {
        CalculatorVo calculatorVo = (CalculatorVo) getVO();
        String screenResult = calculatorVo.getResultVo().getResult();
        switch (calculatorVo.getOperator()) {
            case ADD:
                calculatorVo.setOperand(calculatorVo.getOperand() + Double.parseDouble(screenResult));
                updateStateToResult();
                break;
            case SUBSTRACT:
                calculatorVo.setOperand(calculatorVo.getOperand() - Double.parseDouble(screenResult));
                updateStateToResult();
                break;
            case MULTIPLY:
                calculatorVo.setOperand(calculatorVo.getOperand() * Double.parseDouble(screenResult));
                updateStateToResult();
                break;
            case DIVIDE:
                calculatorVo.setOperand(calculatorVo.getOperand() / Double.parseDouble(screenResult));
                updateStateToResult();
                break;
            default:
                break;
        }
    }

    private void updateStateToResult() {
        CalculatorVo calculatorVo = (CalculatorVo) getVO();
        calculatorVo.getResultVo().setResult(String.valueOf(calculatorVo.getOperand()));
        calculatorVo.setOperator(OperatorEnum.NONE);
        calculatorVo.setState(StateEnum.RESULT);
    }

    public void comaAction() {
        CalculatorVo calculatorVo = (CalculatorVo) getVO();
        String screenResult = calculatorVo.getResultVo().getResult();
        if (calculatorVo.getState() == StateEnum.NEW) {
            calculatorVo.getResultVo().setResult("0.");
            calculatorVo.setState(StateEnum.WAITING);
        } else if (calculatorVo.getState() != StateEnum.RESULT && !screenResult.contains(".")) {
            calculatorVo.getResultVo().setResult(screenResult + ".");
        }
    }

    void eraseAction() {
        CalculatorVo calculatorVo = (CalculatorVo) getVO();
        if (calculatorVo.getState() == StateEnum.RESULT) {
            return;
        }
        String screenResult = calculatorVo.getResultVo().getResult();

        if (screenResult.length() > 1) {
            if ("-".compareTo(screenResult.substring(0, screenResult.length() - 1)) == 0) {
                calculatorVo.getResultVo().setResult("0");
            } else {
                calculatorVo.getResultVo().setResult(screenResult.substring(0, screenResult.length() - 1));
            }
        } else if (screenResult.compareTo("0") != 0) {
            calculatorVo.getResultVo().setResult("0");
            calculatorVo.setState(StateEnum.NEW);
        }
    }

    void clearAction() {
        CalculatorVo calculatorVo = (CalculatorVo) getVO();
        calculatorVo.setOperand(0.0);
        calculatorVo.setOperator(OperatorEnum.NONE);
        calculatorVo.setState(StateEnum.NEW);
        calculatorVo.getResultVo().setResult("0");
    }

    void signAction() {
        CalculatorVo calculatorVo = (CalculatorVo) getVO();
        if (calculatorVo.getState() != StateEnum.RESULT) {
            ResultVo resultVo = calculatorVo.getResultVo();
            if (resultVo.getResult().contains(MINUS)) {
                resultVo.setResult(resultVo.getResult().substring(1));
            } else {
                resultVo.setResult(MINUS + resultVo.getResult());
            }
        }
    }
}
