package taridev.java240.calculette.composite;

import taridev.java240.calculette.vo.ResultVo;
import taridev.swingvu.composite.AbstractCompositeWithHelper;
import taridev.swingvu.exception.AbstractBusinessException;
import taridev.swingvu.factory.GraphicFactory;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class ScreenComposite extends AbstractCompositeWithHelper<ResultVo, ScreenCompositeHelper> {

    private static final Color SCREEN_COLOR = new Color(92, 104, 72, 255);
    private static final String SCREEN_FONT_PATHNAME = "rsc/Calculator.ttf";
    private static final Font SCREEN_FONT = ScreenComposite.createScreenFont();
    private static final float SCREEN_FONT_SIZE = (float) 50.0;
    private static final Border SCREEN_BORDER = BorderFactory.createEmptyBorder(0, 15, 0, 15);

    private static Font createScreenFont() {
        try {
            return Font.createFont(Font.TRUETYPE_FONT,
                    new File(ScreenComposite.SCREEN_FONT_PATHNAME)).deriveFont(ScreenComposite.SCREEN_FONT_SIZE);
        } catch (FontFormatException | IOException e) {
            return new JTextField().getFont().deriveFont(ScreenComposite.SCREEN_FONT_SIZE);
        }
    }

    public ScreenComposite(ResultVo model) throws AbstractBusinessException {
        super(model);
    }

    @Override
    public void innerCreateContent() {
        JTextField screen = GraphicFactory.createTextField(null, getVo(), "result", 10, "Resultat");
        screen.setEditable(false);
        screen.setFocusable(false);
        screen.setFont(ScreenComposite.SCREEN_FONT);
        screen.setBackground(ScreenComposite.SCREEN_COLOR);
        screen.setBorder(BorderFactory.createCompoundBorder(screen.getBorder(), ScreenComposite.SCREEN_BORDER));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;

        setLayout(new GridBagLayout());
        add(screen, gbc);
        screen.setHorizontalAlignment(SwingConstants.RIGHT);
        setBackground(Color.BLACK);
    }

    @Override
    public void mapComponentsToListeners() {
        // Pas d'évenement à gérer...
    }
}
