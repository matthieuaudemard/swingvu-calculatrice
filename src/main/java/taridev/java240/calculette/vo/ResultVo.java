package taridev.java240.calculette.vo;

import taridev.swingvu.vo.AbstractVo;

public class ResultVo extends AbstractVo {

    private String result = "0";

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        changeSupport.firePropertyChange("result", this.result, result);
        this.result = result;
    }
}
