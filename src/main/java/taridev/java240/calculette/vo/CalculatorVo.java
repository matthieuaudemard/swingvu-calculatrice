package taridev.java240.calculette.vo;

import taridev.swingvu.vo.AbstractVo;

public class CalculatorVo extends AbstractVo {

    private ResultVo resultVo = new ResultVo();
    private Double operand = 0.0;
    private OperatorEnum operator = OperatorEnum.NONE;
    private StateEnum state = StateEnum.NEW;

    public ResultVo getResultVo() {
        return resultVo;
    }

    public void setResultVo(ResultVo resultVo) {
        changeSupport.firePropertyChange("result", this.resultVo, resultVo);
        this.resultVo = resultVo;
    }

    public Double getOperand() {
        return operand;
    }

    public void setOperand(Double operand) {
        changeSupport.firePropertyChange("operand", this.operand, operand);
        this.operand = operand;
    }

    public OperatorEnum getOperator() {
        return operator;
    }

    public void setOperator(OperatorEnum operator) {
        changeSupport.firePropertyChange("operator", this.operator, operator);
        this.operator = operator;
    }

    public StateEnum getState() {
        return state;
    }

    public void setState(StateEnum state) {
        changeSupport.firePropertyChange("state", this.state, state);
        this.state = state;
    }
}
