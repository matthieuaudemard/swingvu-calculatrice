package taridev.java240.calculette.vo;

public enum StateEnum {
    NEW,
    RESULT,
    WAITING
}
