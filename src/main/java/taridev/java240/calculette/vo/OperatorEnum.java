package taridev.java240.calculette.vo;

public enum OperatorEnum {

    ADD ("+"),
    SUBSTRACT ("-"),
    MULTIPLY ("\u00D7"),
    DIVIDE ("\u00F7"),
    PLUS_MINUS ("\u00B1"),
    COMMA ("\u002c"),
    CLEAR ("AC"),
    ERASE ("\u2b05"),
    EQUAL ("\u003D"),
    NONE ("none");

    private String litteral;

    OperatorEnum(String litteral) {
        this.litteral = litteral;
    }

    @Override
    public String toString() {
        return litteral;
    }
}
