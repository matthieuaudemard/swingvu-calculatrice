package taridev.java240.calculette;

import taridev.java240.calculette.composite.KeyboardComposite;
import taridev.java240.calculette.composite.ScreenComposite;
import taridev.java240.calculette.vo.CalculatorVo;
import taridev.swingvu.exception.AbstractBusinessException;

import javax.swing.*;
import java.awt.*;

public class App extends JFrame {

    public App() throws AbstractBusinessException {
        CalculatorVo calculatorVo = new CalculatorVo();
        KeyboardComposite clavier = new KeyboardComposite(calculatorVo);
        ScreenComposite screen = new ScreenComposite(calculatorVo.getResultVo());
        setLayout(new BorderLayout());
        getContentPane().add(screen, BorderLayout.NORTH);
        getContentPane().add(clavier, BorderLayout.CENTER);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(300, 300);
        setResizable(false);
    }

    public static void main(String[] args) throws AbstractBusinessException {
        new App().setVisible(true);
    }
}
